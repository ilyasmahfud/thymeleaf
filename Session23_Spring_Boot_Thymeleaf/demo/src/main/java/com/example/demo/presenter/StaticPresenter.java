package com.example.demo.presenter;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class StaticPresenter {

    @GetMapping("/login")
    public String login(Model model){ return "login";}

    @GetMapping("/profile")
    public String profil(Model model){
        return "profil";
    }

    @GetMapping("/catalog")
    public String catalog(Model model){
        return "catalog";
    }

    @GetMapping("/contact")
    public String contactUs(Model model){
        return "contacUs";
    }

}
